package br.edu.ifsc.imc;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        final float[] imc = new float[1];
        Button btnCalcular=(Button) findViewById(R.id.calc);
        btnCalcular.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                TextView txtPeso=(TextView) findViewById(R.id.pesoText);
                TextView txtAltura=(TextView) findViewById(R.id.alturaText);
                TextView resultado= (TextView) findViewById(R.id.imc);
                ImageView imagem= (ImageView) findViewById(R.id.ivPrincipal);

                float peso=Float.parseFloat(txtPeso.getText().toString());
                float altura=Float.parseFloat(txtAltura.getText().toString());
                imc[0] = (float) (peso/Math.pow(altura,2));
                if(imc[0]<18.5){
                    resultado.setText(imc[0]+" - Abaixo do peso");
                    imagem.setImageResource(R.drawable.abaixopeso);
                }else if(imc[0]<25){
                    resultado.setText(imc[0]+" - Peso ideal");
                    imagem.setImageResource(R.drawable.pesonormal);
                }else if(imc[0]<30){
                    resultado.setText(imc[0]+" - Sobrepeso");
                    imagem.setImageResource(R.drawable.sobrepeso);
                }else if(imc[0]<35){
                    resultado.setText(imc[0]+" - Obesidade 1");
                    imagem.setImageResource(R.drawable.ob1);
                }else if(imc[0]<40){
                    resultado.setText(imc[0]+" - Obesidade 2");
                    imagem.setImageResource(R.drawable.ob2);
                }else{
                    resultado.setText(imc[0]+" - Obesidade 3");
                    imagem.setImageResource(R.drawable.ob3);
                }
            }
        });
    }
}
